/***************************************************************************
*=====================      Copyright by Continental AG      ===============
****************************************************************************
* Title        : Swc_PowerFlow_c1.h
*
* Description  : This file contains the implementation of the PowerFlow module.
*
* Environment  : Autosar
*
* Responsible  : I ID R&D NKG
*
* Guidelines   : SMK 4.17
*
* Template name: SWMODxC2.C, Revision 1.1
*
* CASE-Tool    : EasyCASE(C++), Version 6.5
*
* Revision List: (Will be filled by PVCS)
* Archive: $Log$
*
****************************************************************************/

typedef enum
{
    PHEV_enAvailabilityNotUse0 = 0,
    PHEV_enAvailabilityActive = 1,
    PHEV_enAvailabilityNotActive = 2,
    PHEV_enAvailabilityError = 3,
    PHEV_enAvailabilityNotAvailable = 4,
    PHEV_enAvailabilityNotUse,
}PHEV_tenAvailabilityType;


#define PHEV_nPowerFlowMaskTelltlElecDrvrFailr     (1 << 0)
#define PHEV_nPowerFlowMaskTelltlSysHybFailr       (1 << 1)
#define PHEV_nPowerFlowMaskTelltlPwrLoss           /*(1 << 2)*/(0)

//PreEgyMngt
#define PEMState_Invalid            (0)
#define PEMState_Inactive           (1)
#define PEMState_TempInactive       (2)
#define PEMState_Active             (3)

#define PEM_DispOff                 (0)
#define PEM_DispActive              (1)
#define PEM_DispStandby             (2)

