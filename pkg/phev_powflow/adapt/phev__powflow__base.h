#define VAR(vartype,memclass) vartype
#define FUNC(rettype, memclass) rettype

#define TYPEDEF
#define POWC_NormalState     0

typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned long uint32;
typedef unsigned char CDEF_tBool;
typedef CDEF_tBool bool;

#define FALSE 0U
#define TRUE  1U
#define True  1U
#define RTE_E_MAX_AGE_EXCEEDED      64U
#define MSM_UsageMode_Driving       13U
#define MSM_UsageMode_Convenience   2U
#define MSM_UsageMode_Active        1U
#define MSM_UsageMode_Abandoned     0U
#define MSM_CarMode_Normal          0U
#define NULL (void *)0
#define CarModSts1_CarModTrnsp      1U

typedef struct
{
	VAR(uint8, TYPEDEF) u8PrpsnMod;
	VAR(uint8, TYPEDEF) u8AvailabilityOfPrpsnMod;
	VAR(uint8, TYPEDEF) u8Reserved0;
	VAR(uint8, TYPEDEF) u8Reserved1;

}PHEV_tstPowerFlow;

typedef struct
{
	VAR(uint8, TYPEDEF) Msm_u8UsageMode;
	VAR(uint8, TYPEDEF) Msm_u8CarMode;
	VAR(uint8, TYPEDEF) Msm_u8VehicleModeTimeoutFlag;
}Msm_stVehicleModeInfoToApp;

typedef struct
{
	VAR(uint8, TYPEDEF) TelltlBattTraceFailr;
	VAR(uint8, TYPEDEF) TelltlBattTraceCutOff;
	VAR(uint8, TYPEDEF) TelltlElecDrvFailr;
	VAR(uint8, TYPEDEF) TelltlSysHybFailr;
}Ecu_tstHybErrIndcnReq;

typedef struct
{
	VAR(uint8, TYPEDEF)u8Availability;
	VAR(uint8, TYPEDEF)u16Data;
}PHEV_tstRgtvPwr;

typedef struct
{
	VAR(uint16, TYPEDEF) u16Data;
	VAR(uint8, TYPEDEF) u8Availability;
	VAR(uint8, TYPEDEF) u8Value;
}PHEV_tstPreEgyMngtIndcn;

typedef struct
{
	VAR(uint8, TYPEDEF) DispElecPwrSts;
	VAR(uint8, TYPEDEF) DispElecPwrAbs;
}Ecu_tstDispElecPwr;
