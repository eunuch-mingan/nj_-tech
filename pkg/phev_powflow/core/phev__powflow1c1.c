/***************************************************************************
*=====================      Copyright by Continental AG      ===============
****************************************************************************
* Title        : Swc_PowerFlow.c
*
* Description  : This file contains the implementation of the PowerFlow module.
*
* Environment  : Autosar
*
* Responsible  : I ID R&D NKG
*
* Guidelines   : SMK 4.17
*
* Template name: SWMODxC2.C, Revision 1.1
*
* CASE-Tool    : EasyCASE(C++), Version 6.5
*
* Revision List: (Will be filled by PVCS)
* Archive: $Log$
*
****************************************************************************/

/* ==================[Includes]=============================================== */
#include "phev__powflow1ci.h"
#include "../adapt/phev__powflow1c1.h"

/*
* Description   : Phev Carconfig Info
*/
uint8 Phev__u8CarConfig13;

/*req Id 90214*/
/***************************************************************************
* Interface Description    : The function used to set power PrpsnMode value
*
* Parameter                : none
*
* Return Value             : none
****************************************************************************/
void PHEV__vPowerFlowSetPrpsnMode(PHEV_tstPowerFlow* pstPowerFlowIndicated)
{
    uint8  u8UsageMode;
    uint8  u8VehCfgPrm13 = 0;
    uint8  u8VehCfgPrm09 = 0;
    uint8  u8VehCfgPrm03 = 0;
    uint8  u8VehCfgPrm01 = 0;
    uint8  u8SignalValue = 0;
    uint8  u8SignalStatus = 0;
    uint8  u8SignalValueMax = 0;
    uint8  u8SignalDispWhlMotSysPrpsnMod = 0;
    uint8  u8SignalDispWhlMotSysPrpsnModStatus = 0;
    static uint16   PowerFlow_u16Tick = 0;
    static bool PowerFlow_boTimeoutFlag = FALSE;

    Msm_stVehicleModeInfoToApp stGetVehicleModeInfo = { 0 };

    PHEV__vPowerFlowGetCarConfigPrm(PHEV__nPowerFlowVehCfgPrm13, &u8VehCfgPrm13);
    PHEV__vPowerFlowGetCarConfigPrm(PHEV__nPowerFlowVehCfgPrm09, &u8VehCfgPrm09);
    PHEV__vPowerFlowGetCarConfigPrm(PHEV__nPowerFlowVehCfgPrm03, &u8VehCfgPrm03);
    PHEV__vPowerFlowGetCarConfigPrm(PHEV__nPowerFlowVehCfgPrm01, &u8VehCfgPrm01);

    u8SignalDispWhlMotSysPrpsnMod = PHEV__stPowerFlowGetSignalDispWhlMotSysPrpsnMod();
    u8SignalDispWhlMotSysPrpsnModStatus = PHEV__stPowerFlowGetSignalDispWhlMotSysPrpsnModStatus();

    PHEV__vPowerFlowGetVehicleModeInfo(&stGetVehicleModeInfo);
    u8UsageMode = stGetVehicleModeInfo.Msm_u8UsageMode;

    if ((u8VehCfgPrm13 == PHEV__nPowerFlowVehCfgPrm13Pluginhybrid) || (u8VehCfgPrm13 == PHEV__nPowerFlowVehCfgPrm13Hybrid))
    {
        if ((u8UsageMode == MSM_UsageMode_Driving))
        {
            u8SignalValue = PHEV__u8PowerFlowGetPrpsnModFromFlexRay();
            u8SignalStatus = PHEV__u8PowerFlowGetSignalDispOfPrpsnModStatus();
            u8SignalValueMax = PHEV__nPowerFlowPrpsnModMax;

            if ((u8SignalStatus & RTE_E_MAX_AGE_EXCEEDED) == 0)
            {
                PowerFlow_u16Tick = 0;
                PowerFlow_boTimeoutFlag = FALSE;
            }
            else
            {
                PowerFlow_u16Tick++;
                if (PowerFlow__nu16UpInterval <= PowerFlow_u16Tick)
                {
                    PowerFlow_u16Tick = 0;
                    PowerFlow_boTimeoutFlag = TRUE;
                }
            }


            if ((u8VehCfgPrm09 == PHEV__nPowerFlowVehCfgPrm093DHT) && (u8VehCfgPrm03 == PHEV__nPowerFlowVehCfgPrm03E4WD) && ((u8SignalDispWhlMotSysPrpsnModStatus & RTE_E_MAX_AGE_EXCEEDED) == 0))
            {
                if (u8SignalDispWhlMotSysPrpsnMod <= 2)
                {
                    pstPowerFlowIndicated->u8Reserved0 = u8SignalDispWhlMotSysPrpsnMod;
                }
                else
                {
                    pstPowerFlowIndicated->u8Reserved0 = 0;
                }
            }
            else
            {
                pstPowerFlowIndicated->u8Reserved0 = 0;
            }

            if (FALSE == PowerFlow_boTimeoutFlag)
            {
                pstPowerFlowIndicated->u8AvailabilityOfPrpsnMod = PHEV_enAvailabilityActive;

                if (u8SignalValue < u8SignalValueMax) //update for 196965 v4  add new carconfig condition for powerflow delete change
                {
                    pstPowerFlowIndicated->u8PrpsnMod = u8SignalValue;
                }
                else
                {
                    if (u8SignalValue == HEV__nOnlyEngPrpsnAndChrgn)
                    {
                        pstPowerFlowIndicated->u8PrpsnMod = HEV__nOnlyEngPrpsnAndChrgn_R;
                    }
                    else if (u8SignalValue == HEV__nPrpsnMotElecAndEngChrgn)
                    {
                        pstPowerFlowIndicated->u8PrpsnMod = HEV__nPrpsnMotElecAndEngChrgn_R;
                    }
                    else if (u8SignalValue == HEV__nPrpsnMotElecAndEngOn)
                    {
                        pstPowerFlowIndicated->u8PrpsnMod = HEV__nPrpsnMotElecAndEngOn_R;
                    }
                    else
                    {
                        pstPowerFlowIndicated->u8PrpsnMod = 0;
                    }
                }

            }
            else
            {
                pstPowerFlowIndicated->u8PrpsnMod = 0;
                pstPowerFlowIndicated->u8AvailabilityOfPrpsnMod = PHEV_enAvailabilityError;
            }
        }
        else
        {
            pstPowerFlowIndicated->u8AvailabilityOfPrpsnMod = PHEV_enAvailabilityNotActive;
            pstPowerFlowIndicated->u8PrpsnMod = 0;
            pstPowerFlowIndicated->u8Reserved0 = 0;
            PowerFlow_u16Tick = 0;
            PowerFlow_boTimeoutFlag = FALSE;
        }
    }
    else
    {
        pstPowerFlowIndicated->u8AvailabilityOfPrpsnMod = PHEV_enAvailabilityNotAvailable;
        pstPowerFlowIndicated->u8PrpsnMod = 0;
        pstPowerFlowIndicated->u8Reserved0 = 0;
    }

    return;

}

/***************************************************************************
* Interface Description    : The function used to get the  warnning information of powerflow module
*
* Parameter                : none
*
* Return Value             : none
****************************************************************************/
void PHEV__vPowerFlowSetTelltaleWarnInfo(uint8* u8LiightIndicator)
{

}

/***************************************************************************
* Interface Description    : The function used to  Regenerative Power Indicaiton,646023 v1
*
* Parameter                : none
*
* Return Value             : none
****************************************************************************/
void PHEV__vPowerFlowSetRegPowInfo(Msm_stVehicleModeInfoToApp stVehicleMode)
{

}

/***************************************************************************
* Interface Description    : The function used to Predictive Energy Management,539593 v1
*
* Parameter                : none
*
* Return Value             : none
****************************************************************************/
void PHEV__vPowerFlowSetPreEnergyManageInfo(Msm_stVehicleModeInfoToApp stVehicleMode)
{
    
}

/* ==================[Definition of functions with external linkage]========== */
/* ------------------------[runnable entity skeletons]------------------------ */
/***************************************************************************
* Interface Description    : This is the main function of powerflow module
*
* Parameter                : none
*
* Return Value             : none
****************************************************************************/
FUNC(void, RTE_CODE) Rte_PowerFlow_vMain(void)
{
    uint8  u8UvOvStatus = 0;
    static PHEV_tstPowerFlow stPowerFlowIndicated = { 0 };
    static uint8 u8TelltaleWarn = 0;

    Msm_stVehicleModeInfoToApp stVehicleModeInfoToApp = {0,0,0};

    PHEV__vPowerFlowGetVehicleModeInfo(&stVehicleModeInfoToApp);

    //Get Carconfig
    PHEV__vPowerFlowGetCarConfigPrm(13, &Phev__u8CarConfig13);


    PHEV__vPowerFlowSetRegPowInfo(stVehicleModeInfoToApp);
    PHEV__vPowerFlowSetPreEnergyManageInfo(stVehicleModeInfoToApp);

    PHEV__vPowerFlowSetPrpsnMode(&stPowerFlowIndicated);
    PHEV__vPoweeFlowSendInicateData(&stPowerFlowIndicated);

    PHEV__vPowerFlowSetTelltaleWarnInfo(&u8TelltaleWarn);
    PHEV__vPoweeFlowSendTeltleMsgeToWrn(u8TelltaleWarn);
} /* FUNC(void, RTE_CODE) Rte_PowerFlow_vMain (void) */
/*
  ------------------------[runnable-independent API]-------------------------

  Copy and paste the following API to those runnable entity functions where
  you want to use them.

  ------------------------[port handle API]----------------------------------
  ------------------------[per instance memory API]--------------------------
 */

 /** @} doxygen end group definition  */
 /* ==================[end of file]============================================ */
