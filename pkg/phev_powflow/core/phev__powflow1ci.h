/***************************************************************************
*=====================      Copyright by Continental AG      ===============
****************************************************************************
* Title        : Swc_PowerFlow_ci.h
*
* Description  : This file contains the implementation of the PowerFlow module.
*
* Environment  : Autosar
*
* Responsible  : I ID R&D NKG
*
* Guidelines   : SMK 4.17
*
* Template name: SWMODxC2.C, Revision 1.1
*
* CASE-Tool    : EasyCASE(C++), Version 6.5
*
* Revision List: (Will be filled by PVCS)
* Archive: $Log$
*
****************************************************************************/
#include  <stdio.h>
#include "stdlib.h"
#include "../../pkg/phev_powflow/adapt/phev__powflow__base.h"

#ifndef __SWC_POWERFLOW_CI_H__
#define __SWC_POWERFLOW_CI_H__

#define PHEV__vPoweeFlowTimeOutUseFlag      0

#define PHEV__vPowerFlowGetVehicleModeInfo(pstVehicleModeInfo)                         //Get VehicleModeInfo From MSM Moudle
#define PHEV__vPowerFlowGetCarConfigPrm(u16CarConfigNumber,pu8CarConfigValue)          //Get Carconfig from ComAPP Moudle

#define PHEV__vPoweeFlowSendInicateData(pstPHEV_tstPowerFlow)                          //Send Struct pstPHEV_tstPowerFlow out
#define PHEV__vPoweeFlowSendTeltleMsgeToWrn(u8Data)                                    //Send WrnStatus to WRN Moudle 
#define PHEV__u8PowerFlowGetUvOvStatus()                        1                      //Get UvOvStatus  

#define PHEV__u8PowerFlowGetPrpsnModFromFlexRay()                  1                   //Get Signal DispOfPrpsnMod Value   
#define PHEV__u8PowerFlowGetSignalDispOfPrpsnModStatus()                  64U          //Get Signal DispOfPrpsnMod Status      

#define PHEV__pstPowerFlowGetHybErrIndcnReqFromFlexRay()                     0        //Get Signal HybErrIndcnReq Value   
#define PHEV__u8PowerFlowGetSignalHybErrIndcnReqStatus()                       0       //Get Signal HybErrIndcnReq Status

#define PHEV__u8PowerFlowGetTelltlPwrLossFromFlexRay()                          0        //Get Signal TelltlPwrLoss Value
#define PHEV__u8PowerFlowGetSignalTelltlPwrLossStatus()                         0        //Get Signal TelltlPwrLoss Status

#define PHEV__stPowerFlowGetSignalDispElecPwr()                                  0       //Get Signal DispElecPwr Value
#define PHEV__vPowerFlowSendRgtvPwr(stRgtvPwr)                                   0       //Send Struct  stRgtvPwr out
#define PHEV__stPowerFlowGetSignalDispElecPwrTimeOutFlag()                       0       //Get Signal DsipElecPwr Status
#define PHEV__stPowerFlowGetSignalPEMStateTimeOutFlag()                          0       //Get Signal PEMState Status
#define PHEV__stPowerFlowGetSignalPEMState()                                      0      //Get Signal PEMState Value
#define PHEV__stPowerFlowGetSignalPEMFuelSave()                                    0     //Get Signal PEMFuelSave Value
#define PHEV__vPowerFlowSendPreEgyMngtIndcn(stPreEgyMngtIndcn)                     0     //Send Struct stPreEgyMngtIndcn out

#define PHEV__stPowerFlowGetSignalDispWhlMotSysPrpsnMod()                           0    //Get Signal DispWhlMotSysPrpsnMod Value
#define PHEV__stPowerFlowGetSignalDispWhlMotSysPrpsnModStatus()                     0    //Get Signal DispWhlMotSysPrpsnMod Status

#define PHEV__TimeOut(a)                                                                ((a & RTE_E_MAX_AGE_EXCEEDED) > 0)

#define PHEV__Nu8LCFG_EPO                                                               ((uint8)(Wrn_u8GetOTA_LCFG_EPO))


#define PHEV__nPowerFlowVehCfgPrm13                                                     13
#define PHEV__nPowerFlowVehCfgPrm09                                                     9
#define PHEV__nPowerFlowVehCfgPrm03                                                     3
#define PHEV__nPowerFlowVehCfgPrm01                                                     1

#define PHEV__nPowerFlowVehCfgPrm13CombustionEngineOnly                                 0x01U
#define PHEV__nPowerFlowVehCfgPrm13Hybrid                                               0x02U
#define PHEV__nPowerFlowVehCfgPrm13Pluginhybrid                                         0x03U
#define PHEV__nPowerFlowVehCfgPrm13BatteryElectric                                      0x04U
#define PHEV__nPowerFlowVehCfgPrm13Mhev                                                 0x80U
#define PHEV__nPowerFlowVehCfgPrm093DHT                                                 0x8BU
#define PHEV__nPowerFlowVehCfgPrm03E4WD                                                 0x80U

#define HEV__nOnlyEngPrpsnAndChrgn                                             13
#define HEV__nOnlyEngPrpsnAndChrgn_R                                           15
#define HEV__nPrpsnMotElecAndEngChrgn                                          14
#define HEV__nPrpsnMotElecAndEngChrgn_R                                        13
#define HEV__nPrpsnMotElecAndEngOn                                             15
#define HEV__nPrpsnMotElecAndEngOn_R                                           14

#define PHEV__nPowerFlowFlexRaySignalRawDataFactor                                      10

#define PHEV__nPowerFlowDispOfPrpsnPwrPercActRawMin                                     -1000
#define PHEV__nPowerFlowDispOfPrpsnPwrPercActRawMax                                     1000
#define PHEV__nPowerFlowDispOfPrpsnPwrPercActRawOffset                                  0

#define PHEV__nPowerFlowDispOfBrkPwrPercRgnMinRawMin                                    -500
#define PHEV__nPowerFlowDispOfBrkPwrPercRgnMinRawMax                                    500
#define PHEV__nPowerFlowDispOfBrkPwrPercRgnMinRawOffset                                 -50

#define PHEV__nPowerFlowRegenerationGradeBase                                           25
#define PHEV__nPowerFlowRegenerationPercentMax                                          100

#define PHEV__nPowerFlowPrpsnModMax                                                     13
#define PHEV__nPowerFlowDispOfPrpsnModForEvMax                                          7

#define PHEV__nPowerFlowEngFltIndcnFlt                                                  1

#define PHEV__nPowerFlowManiFunctionPeriodMs                                            (80)
#define PHEV__nPowerFlowUsgModKeepTime5sTicks                                           (5 * 1000 / PHEV__nPowerFlowManiFunctionPeriodMs)

#define PHEV__nPowerFlowHybErrIndcnReqRecover5sTicks                                    (5000 / PHEV__nPowerFlowManiFunctionPeriodMs)
#define PHEV__nPowerFlowTelltlPwrLossRecover400msTicks                                  (400 / PHEV__nPowerFlowManiFunctionPeriodMs)

#define PowerFlow__nu16UpInterval                           13

#define PreEgyMngtIndcn_Active 1
#define PreEgyMngtIndcn_Notactive 2
#define PreEgyMngtIndcn_Error 3
#define PreEgyMngtIndcn_Notavailiable  4

#define RgtvPwr_Active  1
#define RgtvPwr_NotActive   2
#define RgtvPwr_Error   3
#define RgtvPwr_Notavailiable   4

#define DispElecPwrAbs_ValueMax 1023
#define DispElecPwrAbs_ShowValueMax 1000

#define PEMFuelSave_Max  100

#endif